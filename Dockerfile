# nginx-centos8
FROM docker.io/library/centos
MAINTAINER Seth Kenlon <skenlon@redhat.com>

ENV NGINX_VERSION=1.14.1

# Set labels used in OpenShift to describe the builder images
LABEL io.k8s.description="Platform for serving HTML files" \
      io.k8s.display-name="Lighttpd 1.4.1" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,html,nginx"

VOLUME /usr/share/nginx/html
VOLUME /etc/nginx

# Install nginx 
RUN yum install nginx -y && yum clean all -y

# TODO (optional): Copy the builder files into /opt/app-root
# COPY ./<builder_folder>/ /opt/app-root/

# TODO: Copy the S2I scripts to image
# COPY ./s2i/bin/ /usr/libexec/s2i

# RUN chown -R 1001:1001 /opt/app-root

# This default user is created in the openshift/base-centos8 image
USER 1001

# TODO: Set the default port for applications built using this image
EXPOSE 80

# TODO: Set the default CMD for the image
# CMD ["/usr/libexec/s2i/usage"]

